<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function bio()
    {
        return view('biodata');
    }
    public function welcome()
    {
        return view('welcome');
    }
    public function kirim(Request $request)
    
    {
       $nama = $request['name'];
       $alamat = $request['alamat'];
       $jenisKelamin = $request['JK'];
       return view('home', ['nama' => $nama, 'alamat' => $alamat, 'jenisKelamin' => $jenisKelamin ]);
    }
}
