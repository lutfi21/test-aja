<?php
require_once('animals.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animals('Shaun');

echo "Nama : " .  $sheep->nama . "<br>"; // "shaun"
echo "Memiliki Kaki : " . $sheep->legs . "<br>" ; // 2
echo " Berdarah Dingin : " . $sheep->coldblooded . "<br><br>"; // false

$frog = new frog('Budug');
echo "Nama : " .  $frog->nama . "<br>"; // "shaun"
echo "Memiliki Kaki : " . $frog->legs . "<br>" ; // 2
echo " Berdarah Dingin : " . $frog->coldblooded . "<br>"; // false
echo " Jump : " . $frog->jump . "<br><br>"; // false

$ape = new ape('Kera Sakti');
echo "Nama : " .  $ape->nama . "<br>"; // "shaun"
echo "Memiliki Kaki : " . $ape->legs . "<br>" ; // 2
echo " Berdarah Dingin : " . $ape->coldblooded . "<br>"; // false
echo " Yell : " . $ape->yell . "<br><br>"; // false
?>