<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h3>Soal Looping</h3>
    <?php
    echo "<h3>Contoh Soal 1</h3>";
    echo "<h4>Looping 1</h4>";
    $i=1;
    while($i <= 19){
        echo $i ." ". "I Love PHP"."<br>";
        $i += 1;
    }
    echo "<h4>Looping 2</h4>";
    $a = 19;
    while($a >= 1){ 
        echo $a." "."I Love PHP"."<br>";
        $a -=1;
    }

    echo "<h3>Contoh No 2</h3>";
    $nomor = [56, 57, 98, 16, 54];
    echo "nomor : ";
    print_r($nomor);
    foreach($nomor as $angka){
        $rest[] = $angka %= 2;
    }
    echo "<br>";
    echo "Sisa Bagi 2 Nomor : ";
    print_r($rest);

    echo "<h3>Contoh Soal No 3</h3>";
    $biotrainer = [
        ["Lutfi" , "Jumat","MTK"],
        ["Rizal" , "Rabu" , "PKN"],
        ["Titi" , "Sabtu" , "HRGA"],

    ];
    foreach($biotrainer as $bio){
       $tampung = [
         "Nama" => $bio[0],
         "Hari" => $bio[1],
        "Matkul" => $bio[2]
        ];
        print_r($tampung);
        echo "<br>";
    }
    echo "<h3>Contoh Soal No 4</h3>";
    for($j=5; $j>=1; $j--){
        for($k=$j; $k <=5; $k++){
            echo "*";
        }
        echo "<br>";
    }



?>
</body>
</html>