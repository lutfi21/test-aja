<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Contoh Array</h1>
    <?php
echo "<h3>Contoh Soal 1 Array</h3>";

$animals = ["Kucing", "Gajah", "Singa", "Kelinci"];
print_r($animals);

echo "<h3> Contoh Soal 2 Array</h3>";
echo " Total Animals Adalah : ".count($animals)."<br>";
echo "<ul>";
echo "<li>". $animals[0]."</li>";
echo "<li>". $animals[1]."</li>";
echo "<li>". $animals[2]."</li>";
echo "<li>". $animals[3]."</li>";
echo "</ul>";

echo "<h3>Contoh Soal 3 Array</h3>";
$biotrainer = [
[ "id" =>  "01" , "Nama" => "Rizki", "Matkul" => "Laravel"],
["id" => "02","Nama" =>"Lutfi", "Matkul" => "Food"],
["id" =>"03","Nama" =>"Stephen","Matkul" =>"Safety"],
];
echo "<pre>";
print_r($biotrainer);
echo "</pre>";
    ?>
</body>
</html>
