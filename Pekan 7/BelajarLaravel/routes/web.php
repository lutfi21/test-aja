<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

Route::get('/welcome', [HomeController::class, 'welcome']);

Route::post('/kirim', [HomeController::class, 'kirim']);

Route::get('/data-table', function(){
    return view('halaman.table');
});

//create data
//menuju ke inputan tambah data
Route::get('/cast/tambah', [CastController::class, 'create']);
//untuk aksi simpan data ke database
Route::post('/cast', [CastController::class, 'store']);

//Read Data
//Route Untuk Menampilkan Semua Data
Route::get('/cast', [CastController::class, 'index']);
//Route Dinamis yang bisa get data dari detail berdasarkan id
route::get('/cast/{id}', [CastController::class, 'show']);

//Update Data
route::get('/cast/{id}/edit', [CastController::class, 'edit']);
//Update data di database berdasarkan id
route::put('/cast/{id}', [CastController::class, 'update']);

//Delete Data
route::delete('/cast/{id}', [CastController::class, 'destroy']);
Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
