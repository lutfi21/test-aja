@extends('layout.master')
@section('judul')
Halaman Tambah Cast
@endsection
@section('content')

<a href="/cast/tambah" class="btn btn-primary my-3">Create Data</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse($casts as $key => $item)
      <tr>
        <th scope="row">{{$key + 1}}</th>
        <td>{{$item->Nama}}</td>
        <td>{{$item->Umur}}</td>
        <td>{{$item->Bio}}</td>
        <td>
            
            <form action="/cast/{{$item->id}}" method="POST">
                @csrf
                @method('delete')
                <a href="/cast/{{$item->id}}" class="btn btn-sm btn-info">Detail</a>
                <a href="/cast/{{$item->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
                <input type="Submit" onclick="return confirm('Apakah Kamu Yakin?')" value="Hapus" class="btn btn-sm btn-danger">
            
            </form>
        </td>
        
      </tr>
      @empty
      <h1>Tidak Ada Data Kategori</h1>
          
      @endforelse
     
    </tbody>
  </table>
@endsection