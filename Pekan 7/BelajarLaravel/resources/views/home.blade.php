@extends('layout.master')
@section('judul')
Halaman Home
@endsection

@section('content')

    <h1>Selamat Datang {{$nama}} {{$alamat}} </h1>
    <p>Jenis kelamin = 
        @if($jenisKelamin === "1" )
        Laki-Laki
        @else
        Perempuan
        @endif
@endsection
