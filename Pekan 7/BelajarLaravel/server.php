<?php

$uri = urldecode(
    parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)
);

// Set path to the public directory
$public_path = __DIR__ . '/public';

// If the request URI matches a file in the public directory, return it
if ($uri !== '/' && file_exists($public_path . $uri)) {
    return false;
}

// Otherwise, require the Laravel bootstrap file
require_once $public_path . '/index.php';
