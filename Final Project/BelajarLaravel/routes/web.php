<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\KritikController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);


Route::get('/welcome', [HomeController::class, 'welcome']);

Route::post('/kirim', [HomeController::class, 'kirim']);

Route::get('/data-table', function(){
    return view('halaman.table');
});
Route::get('/cast/create', function(){
    return view('cast.tampil');
});


    
    
    
Route::middleware(['auth'])->group(function () {
    
    //Read Data
    
    
     //untuk aksi simpan data ke database
     Route::post('/cast', [CastController::class, 'store']);
    
    //Update Data
    route::get('/cast/{id}/edit', [CastController::class, 'edit']);
    //Update data di database berdasarkan id
    route::put('/cast/{id}', [CastController::class, 'update']);
    
    //Delete Data
    route::delete('/cast/{id}', [CastController::class, 'destroy']);

    //create data
    //menuju ke inputan tambah data
    Route::get('/cast/create', [CastController::class, 'create']);
    
    

    Route::resource('profile', ProfileController::class)->only(['index','update']);
    
});
   
    //Route Untuk Menampilkan Semua Data
    Route::get('/cast', [CastController::class, 'index']);
    //Route Dinamis yang bisa get data dari detail berdasarkan id
    route::get('/cast/{id}', [CastController::class, 'show']);
    
    



Auth::routes();



Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


//CRUD Katagori
//untuk aksi simpan data ke database
Route::post('/genre', [GenreController::class, 'store']);
    
//Update Data
route::get('/genre/{id}/edit', [GenreController::class, 'edit']);
//Update data di database berdasarkan id
route::put('/genre/{id}', [GenreController::class, 'update']);

//Delete Data
route::delete('/genre/{id}', [GenreController::class, 'destroy']);

//create data
//menuju ke inputan tambah data
Route::get('/genre/create', [GenreController::class, 'create']);


//Route Untuk Menampilkan Semua Data
Route::get('/genre', [GenreController::class, 'index']);
//Route Dinamis yang bisa get data dari detail berdasarkan id
route::get('/genre/{id}', [GenreController::class, 'show']);

//Create Data Film
    Route::post('/film', [FilmController::class, 'store']);
    
    //Update Data
    route::get('/film/{id}/edit', [FilmController::class, 'edit']);
    //Update data di database berdasarkan id
    route::put('/film/{id}', [FilmController::class, 'update']);
    
    //Delete Data
    route::delete('/film/{id}', [FilmController::class, 'destroy']);

    //create data
    //menuju ke inputan tambah data
    Route::get('/film/tambah', [FilmController::class, 'create']);
     
    //Route Untuk Menampilkan Semua Data
    Route::get('/film', [FilmController::class, 'index']);
    //Route Dinamis yang bisa get data dari detail berdasarkan id
    route::get('/film/{id}', [FilmController::class, 'show']);    

    //Kritik
    

    Route::post('/kritik/{film_id}', [KritikController::class, 'tambah']);
    
