@extends('layout.master')
@section('judul')
Halaman Edit Cast
@endsection
@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group row">
      <label for="inputNama" class="col-sm-2 col-form-label">Nama</label>
      <div class="col-sm-4">
        <input type="text" value="{{$cast->Nama}}" name="Nama" class="form-control" id="inputNama">
      </div>
    </div>
    @error('Nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group row">
      <label for="inputUmur" class="col-sm-2 col-form-label">Umur</label>
      <div class="col-sm-1">
        <input type="text" value="{{$cast->Umur}}" name="Umur" class="form-control" id="inputUmur" pattern="[0-9]+" maxlength="2">
      </div>
    </div>
    @error('Umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group row">
      <label for="inputBio" class="col-sm-2 col-form-label">Bio</label>
      <div class="col-sm-6">
        <textarea name='Bio'  class="form-control" id="inputBio" rows="10">{{$cast->Bio}}</textarea>
      </div>
    </div>
    @error('Bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group row">
      <div class="col-sm-2"></div>
      <div class="col-sm-4">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </form>
  
@endsection