@extends('layout.master')

@section('judul')
Halaman Tampil Cast
@endsection

@section('content')

<div class="row mb-3">
  <div class="col-md-4">
    <form action="/cast" method="GET">
      <div class="input-group">
        <input type="text" name="search" class="form-control" placeholder="Cari Nama Cast" value="{{ old('search') }}">
        <div class="input-group-append">
          <button class="btn btn-primary" type="submit">Cari</button>
        </div>
      </div>
    </form>
  </div>
</div>
@auth
    
<a href="/cast/create" class="btn btn-primary my-3">Create Data</a>

@endauth

<table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse($casts as $key => $item)
      <tr>
        <th scope="row">{{$key + 1}}</th>
        <td>{{$item->Nama}}</td>
        <td>{{$item->Umur}}</td>
        <td>{{$item->Bio}}</td>
        <td>

            
            <form action="/cast/{{$item->id}}" method="POST">
                @csrf
                @method('delete')
                <a href="/cast/{{$item->id}}" class="btn btn-sm btn-info">Detail</a>
                @auth
                    
                <a href="/cast/{{$item->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
                <input type="Submit" onclick="return confirm('Apakah Kamu Yakin?')" value="Hapus" class="btn btn-sm btn-danger">
                @endauth
            
            </form>
        </td>
        
      </tr>
      @empty
      <h1>Tidak Ada Data Kategori</h1>
          
      @endforelse
     
    </tbody>
  </table>
@endsection

@section('scripts')
<script>
  $(document).ready(function() {
    var searchParams = new URLSearchParams(window.location.search);
    if (searchParams.has('search')) {
      var keyword = searchParams.get('search').toLowerCase();
      $('tbody tr').each(function() {
        if ($(this).text().toLowerCase().indexOf(keyword) == -1) {
          $(this).hide();
        }
      });
    }
  });
</script>
@endsection
