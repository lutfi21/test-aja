@extends('layout.master')
@section('judul')
Halaman Detail Genre
@endsection
@section('content')
<h1>{{$genre->Nama}}</h1>

<div class="row">
    @forelse ($genre->films as $item)
    <div class="col-4">
        <div class="card" style="width: 23rem;">
            <img src="{{asset('image/' . $item->Poster)}}" alt="Card image cap">
            <div class="card-body">
                  <h3>{{$item->Judul}}</h3>
                  <p class="card-text">{{Str::limit($item->Ringkasan, 70)}}</p>
                  <a href="/film/{{$item->id}}" class="btn btn-secondary btn-block btn-sm">Detail Film</a>
                  
            </div>
          </div>
    </div>
        
    @empty
        <h3>Genre ini tidak ada Film</h3>
    @endforelse
</div>



<a href="/genre" class="btn btn-secondary btn-sm">Kembali</a>

@endsection