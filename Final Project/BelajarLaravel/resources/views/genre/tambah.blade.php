@extends('layout.master')
@section('judul')
Halaman Tambah Genre
@endsection
@section('content')

<form action="/genre" method="POST">
    @csrf
    
        
    <div class="form-group row">
      <label for="inputNama" class="col-sm-2 col-form-label">Nama</label>
      <div class="col-sm-4">
        <input type="text" name="Nama" class="form-control" id="inputNama">
      </div>
    </div>
    @error('Nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    
    <div class="form-group row">
      <div class="col-sm-2"></div>
      <div class="col-sm-4">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
    
  </form>
  
@endsection