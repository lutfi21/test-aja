@extends('layout.master')
@section('judul')
Halaman Edit Genre
@endsection
@section('content')
<form action="/genre/{{$genre->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group row">
      <label for="inputNama" class="col-sm-2 col-form-label">Nama</label>
      <div class="col-sm-4">
        <input type="text" value="{{$genre->Nama}}" name="Nama" class="form-control" id="inputNama">
      </div>
    </div>
    @error('Nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group row">
      <div class="col-sm-2"></div>
      <div class="col-sm-4">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </form>
  
@endsection