@extends('layout.master')

@section('judul')
Halaman Tampil Film
@endsection

@section('content')

<div class="row mb-3">
  <div class="col-md-4">
    <form action="/film" method="GET">
      <div class="input-group">
        <input type="text" name="search" class="form-control" placeholder="Cari Nama Film" value="{{ old('search') }}">
        <div class="input-group-append">
          <button class="btn btn-primary" type="submit">Cari</button>
        </div>
      </div>
    </form>
  </div>
</div>
@auth
    
<a href="/film/tambah" class="btn btn-primary my-3">Tambah Data</a>
@endauth
<div class= "row">
  @forelse ($film as $item)
  <div class="col-4">
    <div class="card" style="width: 23rem;">
      <img src="{{asset('image/' . $item->Poster)}}" alt="Card image cap">
      <div class="card-body">
            <h3>{{$item->Judul}}</h3>
            <span class="badge badge-info">{{$item->genre->Nama}}</span>
            <p class="card-text">{{Str::limit($item->Ringkasan, 70)}}</p>
            <a href="/film/{{$item->id}}" class="btn btn-secondary btn-block btn-sm">Detail Film</a>
           
               
           <div class="row my-2">
<div class="col">
  @auth
 <a href="/film/{{$item->id}}/edit" class="btn btn-info btn-block btn-sm">Edit</a>
 @endauth
</div>
<div class="col">
<form action="/film/{{$item->id}}" method="POST">
 @csrf
 @method('delete')
 @auth
     
 <input type="submit" class="btn btn-danger btn-block btn-sm" value="Delete">
 @endauth

</form>
</div>


           </div>
     </div>
   </div>

 </div>
     
 @empty
     <h2>Tidak Ada List Film</h2>
 @endforelse

</div>
           


            
            
@endsection
