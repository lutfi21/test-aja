@extends('layout.master')

@section('judul')
Halaman Detail Film
@endsection

@section('content')
    
      <img src="{{asset('image/' . $film->Poster)}}" alt="Card image cap">
      
            <h3>{{$film->Judul}}</h3>
            <p class="card-text">{{$film->Ringkasan}}</p>
            
<hr>
List Komentar

@forelse ($film->kritik as $item)
<div class="media my-3 border">
      <img src="https://dummyimage.com/300/09f.png/fff" class="mr-3" style="border-radius: 50%;" width="200px" alt="...">
      <div class="media-body">
        <h5 class="mt-0 text-primary">{{$item->user->email}}</h5>
        <p>{{$item->content}}</p>
      </div>
    </div>
@empty
    <h4>Tidak Ada Komentar</h4>
@endforelse
<hr>
@auth
    
<form action="/kritik/{{$film->id}}" method="POST">
      @csrf
      <textarea name="content" class="form-control my-3" cols="30" placeholder="isi kritik" rows="10"></textarea>
      @error('content')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group row">
          <label for="inputNama" class="col-sm-1 col-form-label">Poin</label>
          <div class="">
              <input type="text" name="poin" class="form-control" id="inputNama">
          </div>
      </div>
      @error('poin')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <input type="submit" value="kritik">
  </form>
  
@endauth
    <hr>
    <a href="/film" class="btn btn-secondary btn-block btn-sm my-2">Kembali</a>

  
@endsection