@extends('layout.master')
@section('judul')
Halaman Tambah Film
@endsection
@section('content')
<form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group row">
      <label for="inputJudul" class="col-sm-2 col-form-label">Judul</label>
      <div class="col-sm-4">
        <input type="text" name="Judul" value="{{$film->Judul}}"class="form-control" id="inputJudul">
      </div>
    </div>
    @error('Judul')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group row">
      <label for="inputRingkasan" class="col-sm-2 col-form-label">Ringkasan</label>
      <div class="col-sm-6">
        
        <textarea name='Ringkasan'  class="form-control" id="inputRingkasan" cols="50" rows="10">{{$film->Ringkasan}}</textarea>
          
      </div>
    </div>
    @error('Ringkasan')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group row">
      <label for="inputTahun" class="col-sm-2 col-form-label">Tahun</label>
      <div class="col-sm-6">
        <input type="text" name="Tahun" value="{{$film->Tahun}}" class="form-control" id="inputTahun">
      </div>
    </div>
    @error('Tahun')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group row">
    <label for="inputPoster" class="col-sm-2 col-form-label">Poster</label>
    <div class="col-sm-4">
      <input type="file" name="Poster" class="form-control" id="inputPoster">
    </div>
  </div>
  @error('Poster')
  <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group row">
  <label for="inputPoster" class="col-sm-2 col-form-label">Genre</label>
  <div class="col-sm-4">
    <select name='genre_id' class='form-control'>
      <option value="">--Pilih Genre--</option>
   @forelse ($genre as $item)
   @if ($item->id===$film->genre_id)
   <option value="{{$item->id}}" selected>{{$item->Nama}}</option>
       
   @else
   <option value="{{$item->id}}">{{$item->Nama}}</option>   
   @endif
   @empty
       <option value="">Tidak Ada Data Genre</option>
   @endforelse
    </select>
  </div>
</div>
@error('genre_id')
<div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group row">
      <div class="col-sm-2"></div>
      <div class="col-sm-4">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </form>
  
@endsection