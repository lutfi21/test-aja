@extends('layout.master')

@section('judul')
Update Profile
@endsection

@section('content')
<form action="/profile/{{$detailProfile->id}}" method="POST">
    @csrf
    @method('put')

    <div class="form-group">
        <label for="inputUmur">Nama</label>
        <input type="text"  value="{{$detailProfile->user->name}}" class="form-control"   disabled>
        @error('Umur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>

    <div class="form-group">
        <label for="inputUmur">Email</label>
        <input type="text"  value="{{$detailProfile->user->email}}" class="form-control"  disabled>
        @error('Umur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    
    <div class="form-group">
        <label for="inputUmur">Umur</label>
        <input type="text" name="Umur" value="{{$detailProfile->Umur}}" class="form-control"  style="width: 80px";>
        @error('Umur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    
    <div class="form-group">
        <label for="inputAlamat">Alamat</label>
        <textarea name="Alamat" class="form-control"   style="box-shadow: 0 0 10px rgba(0, 0, 0, 0.2) inset; border-radius: 5px; padding: 10px; border: none; background: none; resize: none; height: 150px;">
    {{$detailProfile->Alamat}}</textarea>
        @error('Alamat')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    
    <div class="form-group">
        <label for="inputBio">Biodata</label>
        <textarea name="Bio" class="form-control"  style="box-shadow: 0 0 10px rgba(0, 0, 0, 0.2) inset; border-radius: 5px; padding: 10px; border: none; background: none; resize: none; height: 150px;">
    {{$detailProfile->Bio}}</textarea>
        @error('Bio')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
    
</form>

<style>
    textarea:focus {
        outline: none;
    }
    textarea::after {
        content: "";
        position: absolute;
        z-index: -1;
        top: 8px;
        bottom: 8px;
        left: 8px;
        right: 8px;
        box-shadow: inset 0 0 10px rgba(0, 0, 0, 0.2);
        pointer-events: none;
    }
</style>

@endsection
