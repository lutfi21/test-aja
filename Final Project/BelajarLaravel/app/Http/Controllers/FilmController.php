<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Film;
use App\Models\Genre;
use File;

class FilmController extends Controller
{

  public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }
    public function create()
    {
      $genre = Genre::get();
      return view('film.tambah', ['genre' => $genre]);
    }
    public function store(Request $request)
    {
 
     
       $request->validate([
         'Judul' => 'required',
         'Ringkasan' => 'required',
         'Tahun'  => 'required|numeric',
         'genre_id' => 'required',
         'Poster'  => 'required|image|mimes:jpeg,png,jpg|max:2048',

       ]);

       $fileName = time().'.'.$request->Poster->extension();
       $request->Poster->move(public_path('image'), $fileName);

       $film = New Film;
       $film->Judul = $request->Judul;
       $film->Ringkasan = $request->Ringkasan;
       $film->Tahun = $request->Tahun;
       $film->genre_id = $request->genre_id;
       $film->Poster = $fileName;
       $film->save();

       return redirect('/film');
    }
    public function index()
   {
    
    $film = Film::all();
    return view('film.tampil', ['film'=>$film]);

   }
   public function show($id)
   {
    $film = Film::find($id);
    return view('film.detail', ['film'=>$film]);
   }

   public function edit($id)
    {
      $film = Film::find($id);
      $genre = Genre::get();
        return view('film.edit', ['film'=>$film, 'genre'=>$genre]);
    }
    public function update($id, Request $request)
   {
    $request->validate([
      'Judul' => 'required',
      'Ringkasan' => 'required',
      'Tahun'  => 'required|numeric',
      'genre_id' => 'required',
      'Poster'  => 'image|mimes:jpeg,png,jpg|max:2048',
    ]);

    $film = Film::find($id);

    if ($request->has('Poster')) {
      $path = 'image/';
      File::delete($path. $film->Poster);

      $fileName = time().'.'.$request->Poster->extension();
       $request->Poster->move(public_path('image'), $fileName);
       $film->Poster =$fileName;
       $film->save();
    }
 
    $film->Judul = $request['Judul'];
    $film->Ringkasan = $request['Ringkasan'];
    $film->Tahun = $request['Tahun'];
    $film->genre_id = $request['genre_id'];
    $film->save();
    return redirect('/film');
    
   }

   public function destroy($id)
   {
    $film = Film::find($id);
    
    $path = 'image/';
    File::delete($path. $film->Poster);

    $film->delete();
    return redirect('/film');
   }
}
