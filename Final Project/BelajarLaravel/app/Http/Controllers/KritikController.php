<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kritik;
use Illuminate\Support\Facades\Auth;

class KritikController extends Controller
{
    public function tambah(request $request, $id) 
    {
        $request->validate([
            'content' => 'required',
            'poin' => 'required|numeric',
            
          ]);
          $iduser = Auth::id();
          $kritik = New Kritik;
        
        $kritik->user_id = $iduser;
       $kritik->film_id = $id;
       $kritik->content = $request->content;
       $kritik->poin = $request->poin;
       $kritik->save();

       return redirect('/film/'. $id);
    }
}
