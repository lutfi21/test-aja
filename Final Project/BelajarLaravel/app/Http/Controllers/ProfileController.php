<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;
class ProfileController extends Controller
{
    public function index(){
        $iduser = Auth::id();
        $detailProfile = Profile::where('user_id', $iduser)->first();
        return view('profile.index',['detailProfile'=>$detailProfile]);
}
public function update(Request $request, $id)
   {
     $request->validate([
         'Umur' => 'required',
         'Bio'  => 'required',
         'Alamat' => 'required',
      ]);
      
      $profile = Profile::find ($id); 
      $profile->Umur = $request->Umur;
      $profile->Bio = $request->Bio;
      $profile->Alamat = $request->Alamat;
      $profile->user_id = Auth::id(); // tambahkan baris ini
        $profile->save();
        
      return redirect('/profile');
   }
}
