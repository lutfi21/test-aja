<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cast;

class CastController extends Controller
{

  public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }
    public function create()
   {
        return view('cast.tambah');
   }
   public function store(Request $request)
   {

    
      $request->validate([
        'Nama' => 'required',
        'Umur' => 'required',
        'Bio'  => 'required',
      ]);
      
      Cast::create([
        'Nama' => $request['Nama'],
        'Umur' => $request['Umur'],
        'Bio' => $request['Bio'],
      ]);
        
      return redirect('/cast');
   }
   public function index()
   {
    $casts = Cast::all();
    return view('cast.tampil', ['casts'=>$casts]);

   }


   public function show($id)
   {
    $cast = Cast::where('id', $id)->first();
    return view('cast.detail', ['cast'=>$cast]);
   }

   public function edit($id)
   {
    $cast = Cast::where('id', $id)->first();
    return view('cast.edit', ['cast'=>$cast]);
   }

   public function update($id, Request $request)
   {
    $request->validate([
      'Nama' => 'required',
      'Umur' => 'required',
      'Bio'  => 'required',
    ]);

    $cast = Cast::find($id);
 
    $cast->Nama = $request['Nama'];
    $cast->Umur = $request['Umur'];
    $cast->Bio = $request['Bio'];
 
    $cast->save();
    return redirect('/cast');
    
   }

   public function destroy($id)
   {
    $cast = Cast::find($id);
 
    $cast->delete();
    return redirect('/cast');
   }

   


}
